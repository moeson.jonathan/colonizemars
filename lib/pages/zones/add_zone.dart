import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:multiselect/multiselect.dart';

class AddZone extends StatefulWidget {
  const AddZone({super.key});
  @override
  State<AddZone> createState() => _AddZoneState();
}

class _AddZoneState extends State<AddZone> {
  final nameController = TextEditingController();
  final lieuxController = TextEditingController();
  final dateController = TextEditingController();
  List<String> testselect = [];
  final Stream<QuerySnapshot> _MineraisStream =
      FirebaseFirestore.instance.collection('Minerais').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _MineraisStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          // print(snapshot.error);
          if (snapshot.hasError) {
            return const Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Text("Loading");
          }

          return Scaffold(
              appBar: AppBar(
                title: const Text("Add Zone"),
              ),
              body: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(children: [
                  ListTile(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                          side: const BorderSide(
                              color: Color.fromARGB(77, 0, 0, 0), width: 4)),
                      title: TextField(
                        obscureText: true,
                        decoration: const InputDecoration(
                            border: InputBorder.none, labelText: "Nom"),
                        controller: nameController,
                      )),
                  ListTile(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                          side: const BorderSide(
                              color: Color.fromARGB(77, 0, 0, 0), width: 4)),
                      title: TextField(
                        obscureText: true,
                        decoration: const InputDecoration(
                            border: InputBorder.none, labelText: "Lieux"),
                        controller: lieuxController,
                      )),
                  ListTile(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                          side: const BorderSide(
                              color: Color.fromARGB(77, 0, 0, 0), width: 4)),
                      title: TextField(
                        obscureText: true,
                        decoration: const InputDecoration(
                            border: InputBorder.none, labelText: "Date"),
                        controller: nameController,
                      )),
                  DropDownMultiSelect(
                    onChanged: (List<String> x) {
                      setState(() {
                        testselect = x;
                      });
                    },
                    options: ["", "", ""],
                    selectedValues: testselect,
                    whenEmpty: 'Select Something',
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size.fromHeight(50),
                    ),
                    onPressed: () {
                      FirebaseFirestore.instance.collection("Zones").add({
                        'Nom': nameController.value.text,
                        'Lieux': lieuxController.value.text,
                        "Date": dateController.value.text,
                      });
                      Navigator.pop(context);
                    },
                    child: const Text("Ajouter"),
                  )
                ]),
              ));
        });
  }
} 
       // NumberInputPrefabbed.roundedButtons(
          //   controller: mineraiController,
          //   incDecBgColor: Colors.amber,
          //   buttonArrangement: ButtonArrangement.incRightDecLeft,
          // ),


