import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'add_zone.dart';

class ZonesShow extends StatefulWidget {
  const ZonesShow({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _ZonesShowState createState() => _ZonesShowState();
}

class _ZonesShowState extends State<ZonesShow> {
  final Stream<QuerySnapshot> _ZonesStream =
      FirebaseFirestore.instance.collection('Zones').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _ZonesStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        // print(snapshot.error);
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text("Loading");
        }

        return Scaffold(
            appBar: AppBar(
              title: const Text('Liste des Zones'),
              leading: IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) {
                      return const AddZone();
                    },
                    fullscreenDialog: true,
                  ));
                },
              ),
            ),
            body: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> zones =
                    document.data()! as Map<String, dynamic>;
                return Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(children: [
                    // SizedBox(
                    //   width: 100,
                    //   child: Image.network(Zones["poster"]),
                    // ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            zones['Nom'],
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            zones['Date'],
                          ),

                          // Row(
                          //   children: [
                          // for (final test in Zones["arraytest"])
                          //   Padding(
                          //     padding: const EdgeInsets.only(right: 5),
                          //     child: Chip(
                          //       backgroundColor: Colors.lightBlue,
                          //       label: Text(test.toString()),
                          //     ),
                          //   )
                          //   ],
                          // )
                        ],
                      ),
                    )
                  ]),
                );
              }).toList(),
            ));
      },
    );
  }
}






        // SizedBox(
                  //   width: 100,
                  //   child: Image.network(zones["poster"]),
                  // ),
  // Row(
                        //   children: [
                        //     for (final test in zones["arraytest"])
                        //       Padding(
                        //         padding: const EdgeInsets.only(right: 5),
                        //         child: Chip(
                        //           backgroundColor: Colors.lightBlue,
                        //           label: Text(test.toString()),
                        //         ),
                        //       )
                        //   ],
                        // )