import 'package:colonizemars/pages/minerais/add_minerai.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MineraisShow extends StatefulWidget {
  const MineraisShow({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _MineraisShowState createState() => _MineraisShowState();
}

class _MineraisShowState extends State<MineraisShow> {
  final Stream<QuerySnapshot> _MineraisStream =
      FirebaseFirestore.instance.collection('Minerais').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _MineraisStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        // print(snapshot.error);
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text("Loading");
        }

        return Scaffold(
            appBar: AppBar(
              title: const Text('Liste des Minerais'),
              leading: IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) {
                      return const AddMinerai();
                    },
                    fullscreenDialog: true,
                  ));
                },
              ),
            ),
            body: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> minerais =
                    document.data()! as Map<String, dynamic>;
                return Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Chip(
                              label: Text(
                            minerais['Nom'],
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          )),
                          Text(
                            minerais['Description'],
                          ),
                        ],
                      ),
                    )
                  ]),
                );
              }).toList(),
            ));
      },
    );
  }
}


// SizedBox(
                    //   width: 100,
                    //   child: Image.network(Minerais["poster"]),
                    // ),
  // Row(
                          //   children: [
                          // for (final test in Minerais["arraytest"])
                          //   Padding(
                          //     padding: const EdgeInsets.only(right: 5),
                          //     child: Chip(
                          //       backgroundColor: Colors.lightBlue,
                          //       label: Text(test.toString()),
                          //     ),
                          //   )
                          //   ],
                          // )