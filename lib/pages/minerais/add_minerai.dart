import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AddMinerai extends StatefulWidget {
  const AddMinerai({super.key});
  @override
  State<AddMinerai> createState() => _AddMineraiState();
}

class _AddMineraiState extends State<AddMinerai> {
  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  // List<String> testselect = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Minerai"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(children: [
          ListTile(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                  side: const BorderSide(
                      color: Color.fromARGB(77, 0, 0, 0), width: 4)),
              title: TextField(
                decoration: const InputDecoration(
                    border: InputBorder.none, labelText: "Nom"),
                controller: nameController,
              )),
          ListTile(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                  side: const BorderSide(
                      color: Color.fromARGB(77, 0, 0, 0), width: 4)),
              title: TextField(
                decoration: const InputDecoration(
                    border: InputBorder.none, labelText: "Description"),
                controller: descriptionController,
              )),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              minimumSize: const Size.fromHeight(50),
            ),
            onPressed: () {
              FirebaseFirestore.instance.collection("Minerais").add({
                'Nom': nameController.value.text,
                "Description": descriptionController.value.text,
              });
              Navigator.pop(context);
            },
            child: const Text("Ajouter"),
          )
        ]),
      ),
    );
  }
}



// NumberInputPrefabbed.roundedButtons(
          //   controller: mineraiController,
          //   incDecBgColor: Colors.amber,
          //   buttonArrangement: ButtonArrangement.incRightDecLeft,
          // ),

          // DropDownMultiSelect(
          //   onChanged: (List<String> x) {
          //     setState(() {
          //       testselect = x;
          //     });
          //   },
          //   options: [],
          //   selectedValues: testselect,
          //   whenEmpty: 'Select Something',
          // ),
    